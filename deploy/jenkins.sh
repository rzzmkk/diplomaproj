#!/bin/bash

export WORKSPACE=`pwd`

if [ ! -f env/bin/activate ]; then
    # Create virtualenv
    virtualenv -p python3 env

    # Activate virtualenv
    source env/bin/activate

    # Install Requirements
    pip install --upgrade pip
    pip install -r env/requirements.txt
else
	echo "file env/bin/activate found!"
fi

if [ -f core/config.py ]; then
    # Activate virtualenv
    source env/bin/activate

    # Install Requirements
    pip install --upgrade pip
    pip install -r env/requirements.txt

    # Run makemigrations and migrate
    python manage.py makemigrations
    python manage.py migrate
    python manage.py collectstatic --noinput
    python manage.py runcrons
else
	echo "file core/config.py not found!"
fi