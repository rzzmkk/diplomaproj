# -*- coding: utf-8 -*-

# future
from __future__ import unicode_literals

# standard library

# third-party
from import_export import fields, resources
from import_export.admin import *
from rangefilter.filter import DateRangeFilter

# Django
from django.contrib import admin

# local Django
from mainModule.models import *

# constant
from core.settings import LIST_PER_PAGE_SET


class  AuthenticationResource(resources.ModelResource):
    class Meta:
        model =  Authentication


class AuthenticationAdmin(ImportExportActionModelAdmin, admin.ModelAdmin):
    resource_class = AuthenticationResource

    list_per_page = LIST_PER_PAGE_SET

    exclude = ('id',)
    readonly_fields = ('created_at', 'updated_at',)

    list_display = ('id', 'created_at', 'updated_at',)
    list_display_links = ('id',)

    list_filter = (
        ('created_at', DateRangeFilter),
        ('updated_at', DateRangeFilter),
    )

    search_fields = ('id',)


admin.site.register(Authentication, AuthenticationAdmin)


class  UserResource(resources.ModelResource):
    class Meta:
        model =  User


class UserAdmin(ImportExportActionModelAdmin, admin.ModelAdmin):
    resource_class = UserResource

    list_per_page = LIST_PER_PAGE_SET

    exclude = ('id',)
    readonly_fields = ('created_at', 'updated_at',)

    list_display = ('id', 'created_at', 'updated_at',)
    list_display_links = ('id',)

    list_filter = (
        ('created_at', DateRangeFilter),
        ('updated_at', DateRangeFilter),
    )

    search_fields = ('id','phone',)


class OrderResource(resources.ModelResource):
    class Meta:
        model = Order


class OrderAdmin(ImportExportActionModelAdmin, admin.ModelAdmin):
    resource_class = OrderResource

    list_per_page = LIST_PER_PAGE_SET

    exclude = ('id',)
    readonly_fields = ('created_at', 'updated_at',)

    list_display = ('id','title', 'created_at', 'updated_at',)
    list_display_links = ('id',)

    list_filter = (
        ('created_at', DateRangeFilter),
        ('updated_at', DateRangeFilter),
    )

    search_fields = ('id', 'title',)


admin.site.register(User, UserAdmin)
admin.site.register(Order, OrderAdmin)
