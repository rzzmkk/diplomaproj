# -*- coding: utf-8 -*-

# future
from __future__ import unicode_literals

# standard library
import os
from uuid import uuid4

# third-party
import random
from random import randint
from decimal import Decimal


# Django
from django.apps import apps
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core.mail import send_mail
from django.utils.crypto import get_random_string
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator, MaxLengthValidator, MinValueValidator, MaxValueValidator

# local Django
from mainModule.smsc import SMSC


class Authentication(models.Model):
    id = models.UUIDField(
        primary_key=True,
        db_index=True,
        editable=False,
        default=uuid4,)
    phone = models.CharField(
        unique=True,
        max_length=10,
        validators=[MinLengthValidator(10),MaxLengthValidator(11)],
        verbose_name=(u'Номер'),)
    code = models.CharField(
        max_length=6,
        verbose_name=(u'Код'),)
    password = models.CharField(
        null=True, 
        blank=True,
        max_length=255, 
        verbose_name=(u'Пароль'),)

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=(u'Дата создания'),)
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=(u'Дата обновления'),)

    class Meta:
        verbose_name = (u'авторизация')
        verbose_name_plural = (u'Список авторизационных данных')

    def __str__(self):
        return self.phone
    def save(self, *args, **kwargs):
        if self._state.adding ==True:
            if self.phone is not None:
                self.code = self.generate_code()
                self.send_sms(self.code)
        return super (Authentication, self).save(*args, **kwargs)

    def generate_code(self):
        return get_random_string(length=4, allowed_chars='1234567890')

    def send_sms(self, code):
        return SMSC().send_sms('+7' + self.phone, str(code))


class User(models.Model):
    id = models.UUIDField(
        primary_key=True, 
        db_index=True,
        editable=False,
        default=uuid4,)
    name = models.CharField(
        null=True,
        blank=True,
        max_length=255, 
        verbose_name=(u'Имя'),)
    surname = models.CharField(
        null=True,
        blank=True,
        max_length=255, 
        verbose_name=(u'Фамилия'),)
    gender_choises = (
        (1, u'Мужской'),
        (2, u'Женский'),
    )
    about_me=models.CharField(
        null=True,
        blank=True,
        max_length=255, 
        verbose_name=(u'О себе'),)
    gender = models.PositiveIntegerField(
        null=True,
        blank=True,
        choices=gender_choises,
        verbose_name=(u'Пол'),)
    status_choises = (
        (1, u'Активный'),
        (2, u'Неактивный'),
        (3, u'Блокирован'),
    )
    avatar = models.FileField(
        upload_to = 'pic_folder/avatars',
        default = 'pic_folder/None/no-img.jpg',
        verbose_name = (u'Изображения'),
        blank = True,
    )
    age = models.CharField(
        default=18, 
        max_length=50, 
        verbose_name=(u'Возраст')
    )
    bad_habits = models.CharField(
        verbose_name=(u'Плохие привычки'), 
        blank=True,
        null=True, 
        max_length=150
    )
    preferences = models.CharField(
        verbose_name=(u'Предпочтения'), 
        null=True,
        blank=True, 
        max_length=50
    )
    status = models.PositiveIntegerField(
        default=1,
        choices=status_choises,
        verbose_name=(u'Статус'),)
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=(u'Дата создания'),)
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=(u'Дата обновления'),)

    authentication = models.OneToOneField(
        'Authentication', 
        on_delete=models.CASCADE,
        verbose_name=(u'Авторизация'),)

    class Meta:
        verbose_name = (u'пользователь')
        verbose_name_plural = (u'Список пользователей')

    def __str__(self):
        return self.authentication.phone


class Order(models.Model):
    id = models.UUIDField(
        primary_key=True,
        db_index=True,
        editable=False,
        default=uuid4,
    )
    type_choices = (
        (1, u'Ищу'),
        (2, u'Ищу'),
    )
    order_type = models.PositiveIntegerField(
        default=2,
        choices=type_choices,
        verbose_name=(u'Тип объявления'),
    )
    title = models.CharField(
        max_length=100,
        verbose_name = (u'Тайтл'),
        default = ""
    )
    city = models.CharField(
        max_length=50,
        verbose_name = (u'Город'),
        default="Алматы",
    )
    description=models.CharField(
        max_length=255,
        verbose_name=(u'Описание'),
        default="Офигенная хата",
    )
    room_number = models.CharField(
        null=True,
        blank=True,
        default=3,
        max_length=10,
        verbose_name=(u'Комнатность'),
    )
    room_number_min = models.CharField(
        null=True,
        blank=True,
        default=3,
        max_length=10,
        verbose_name=(u'Минимальная комнатность'),
    )
    room_number_max = models.CharField(
        null=True,
        blank=True,
        default=3,
        max_length=10,
        verbose_name=(u'Максимальная комнатность'),
    )
    price = models.CharField(
        null=True,
        blank=True,
        default=0,
        max_length=10,
        verbose_name=(u'Цена')
    )
    price_min = models.CharField(
        null=True,
        blank=True,
        default=0,
        max_length=10,
        verbose_name=(u'Цена min')
    )
    price_max = models.CharField(
        null=True,
        blank=True,
        default=0,
        max_length=10,
        verbose_name=(u'Цена max')
    )
    requirements = models.CharField(
        max_length=50,
        verbose_name = (u'Требования к подселяющемуся'),
        default="Чистый"
    )
    address = models.CharField(
        max_length=50,
        verbose_name = (u'Адрес'),
        default="Абая"
    )
    fixtype_choices = (
        (1, u'Евроремонт'),
        (2, u'Косметический ремонт'),
        (3, u'Капитальный'),
        (4, u'Без ремонта')
    )
    fix_type = models.PositiveIntegerField(
        verbose_name = (u'Тип ремонта'),
        choices = fixtype_choices,
        default=2
    )
    roommates_needed= models.CharField(
        null=True,
        blank=True,
        verbose_name=(u'Сожителей нужно'),
        max_length=10,
        default=None
    )
    roommates_needed_min= models.CharField(
        null=True,
        blank=True,
        verbose_name=(u'Сожителей нужно min'),
        max_length=10,
        default=None
    )
    roommates_needed_max= models.CharField(
        null=True,
        blank=True,
        verbose_name=(u'Сожителей нужно max'),
        max_length=10,
        default=None
    )
    images = models.FileField(
        upload_to = 'pic_folder/%Y/%m/%d/',
        default = 'pic_folder/None/no-img.jpg',
        verbose_name = (u'Изображения квартиры'),
        blank = True,
    )
    gender_choices=(
        (1, u'Девушка '),
        (2, u'Парень '),
    )
    roommate_gender = models.PositiveIntegerField(
        choices=gender_choices,
        default=3,
        verbose_name=(u'Гендер сожителя'),)
    status_choices = (
        (1, u'Активное'),
        (2, u'Неактивное'),
        (3, u'Блокированное'),
    )
    status = models.PositiveIntegerField(
        default=1,
        choices=status_choices,
        verbose_name=(u'Статус объявления'),)
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=(u'Дата создания'),)
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=(u'Дата обновления'),)
    owner = models.ForeignKey(
        'User',
        on_delete=models.CASCADE,
        verbose_name=(u'Владелец'),)
    def __str__(self):
        return (str(self.owner.name) + ' ' + str(self.title))
    class Meta:
        verbose_name = (u'объявление')
        verbose_name_plural = (u'Список объявлении')

