# -*- coding: utf-8 -*-

# future
from __future__ import unicode_literals

# standard library
import uuid
from datetime import date

# third-party
import django_excel as excel
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import JSONParser
from fcm_django.models import FCMDevice

# Django
from django import forms
from django.conf import settings
from django.contrib.sessions.models import Session
from django.http import (Http404, HttpResponse, HttpResponseBadRequest, JsonResponse)
from django.shortcuts import redirect, render, render_to_response
from django.template import Context, RequestContext
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token

# local Django
from mainModule.models import *
from mainModule.serializers import *
from mainModule.filters import *


class AuthenticationList(APIView):
    def get(self, request, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        if 'phone' in request.GET:
            queryset = Authentication.objects.filter(phone=request.GET['phone'])
            if queryset.exists():
                is_checked = False
                if str(request.META.get('HTTP_TYPE')).lower() == 'user':
                    is_checked = True
                    if User.objects.filter(authentication__phone=request.GET['phone']).exists():
                        return Response({'detail': 'exists'}, status=status.HTTP_302_FOUND)

                if is_checked:
                    serializer = AuthenticationSerializer(queryset, many=True)
                    return Response({'data': serializer.data}, status=status.HTTP_200_OK)
                else:
                    return Response({'detail': 'invalid header'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({'detail': 'not found'}, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response({'detail': 'invalid phone'}, status=status.HTTP_400_BAD_REQUEST)


    def post(self, request, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        if 'phone' in request.data:
            if Authentication.objects.filter(phone=request.data['phone']).exists():
                is_checked = False
                if str(request.META.get('HTTP_TYPE', '')).lower() == 'user':
                    is_checked = True
                    if User.objects.filter(authentication__phone=request.data['phone']).exists():
                        return Response({'detail': 'exists'}, status=status.HTTP_302_FOUND)
                if str(request.META.get('HTTP_TYPE', '')).lower() == 'partner':
                    is_checked = True
                    if Partner.objects.filter(authentication__phone=request.data['phone']).exists():
                        return Response({'detail': 'exists'}, status=status.HTTP_302_FOUND)

                if is_checked:
                    authentication = Authentication.objects.get(phone=request.data['phone'])
                    authentication.code = authentication.generate_code()
                    authentication.send_sms(authentication.code)
                    authentication.save()
                    serializer = AuthenticationPostSerializer(authentication)
                    return Response({'data': serializer.data}, status=status.HTTP_200_OK)
                else:
                    return Response({'detail': 'invalid header'}, status=status.HTTP_400_BAD_REQUEST)

            else:
                serializer = AuthenticationPostSerializer(data=request.data.copy())
                if serializer.is_valid():
                    serializer.save()
                    return Response({'data': serializer.data}, status=status.HTTP_200_OK)
                else:
                    return Response({'data': serializer.errors}, status=status.HTTP_206_PARTIAL_CONTENT)
        else:
            return Response({'detail': 'invalid phone'}, status=status.HTTP_400_BAD_REQUEST)


class AuthenticationDetail(APIView):
    def get(self, request, pk, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            serializer = AuthenticationGetSerializer(Authentication.objects.get(id=pk))
            return Response({'data': serializer.data}, status=status.HTTP_200_OK)
        except Authentication.DoesNotExist:
            return Response({'detail': 'not found'}, status=status.HTTP_404_NOT_FOUND)


    def put(self, request, pk, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            serializer = AuthenticationPutSerializer(Authentication.objects.get(id=pk), data=request.data.copy(), partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'data': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'data': serializer.errors}, status=status.HTTP_206_PARTIAL_CONTENT)
        except Authentication.DoesNotExist:
            return Response({'detail': 'not found'}, status=status.HTTP_404_NOT_FOUND)


class UserList(APIView):
    def get(self, request, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        if 'phone' and 'password' in request.GET:
            if User.objects.filter(authentication__phone=request.GET['phone'], authentication__password=request.GET['password']).exists():
                user = User.objects.get(authentication__phone=request.GET['phone'], authentication__password=request.GET['password'])
                serializer = UserGetSerializer(user)
                return Response({'data': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'invalid phone or password'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'phone or password not found'}, status=status.HTTP_404_NOT_FOUND)


    def post(self, request, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        serializer = UserPostSerializer(data=request.data.copy())
        if serializer.is_valid() and 'code' in request.data:
            if Authentication.objects.filter(id=request.data['authentication'], code=request.data['code']).exists():
                serializer.save()
                user = User.objects.get(authentication__id=request.data['authentication'])
                serializer = UserSerializer(user)
                return Response({'data': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'invalid code'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'data': serializer.errors}, status=status.HTTP_206_PARTIAL_CONTENT)


class UserDetail(APIView):
    def get(self, request, pk, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            serializer = UserGetSerializer(User.objects.get(id=pk))
            return Response({'data': serializer.data}, status=status.HTTP_200_OK)
        except User.DoesNotExist:
            return Response({'detail': 'not found'}, status=status.HTTP_404_NOT_FOUND)


    def put(self, request, pk, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            user = User.objects.get(id=pk)
            serializer = UserPutSerializer(user, data=request.data.copy(), partial=True)
            if serializer.is_valid():
                serializer.save()
                serializer = UserGetSerializer(user)
                return Response({'data': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'data': serializer.errors}, status=status.HTTP_206_PARTIAL_CONTENT)
        except User.DoesNotExist:
            return Response({'detail': 'not found'}, status=status.HTTP_404_NOT_FOUND)

class OrderList(APIView):
    def get(self, request, format=None):
        serializer = OrderGetSerializer()

        current_page = 0
        if 'current_page' in request.GET:
            current_page = int(request.GET['current_page'])
        
        price_min = 0
        if 'price_min' in request.GET:
            price_min = int(request.GET['price_min'])

        
        price_max = 0
        if 'price_max' in request.GET:
            price_max = int(request.GET['price_max'])

        roommates_min = 0
        if 'roommates_min' in request.GET:
            roommates_min = int(request.GET['roommates_min'])

        roommates_max = 0
        if 'roommates_max' in request.GET:
            roommates_max = int(request.GET['roommates_max'])

        per_page = 99999
        if 'per_page' in request.GET:
            per_page = int(request.GET['per_page'])

        order_type = 2
        if 'order_type' in request.GET:
            order_type = int(request.GET['order_type'])
            try:
                queryset=Order.objects.all()
                queryset=queryset.filter(order_type=order_type)
                queryset=queryset[(current_page*per_page):((current_page+1)*per_page)]
                serializer = OrderGetSerializer(queryset, many=True)
                return Response({'current_page': current_page, 'per_page': per_page,'data': serializer.data}, status=status.HTTP_200_OK)
            except Order.DoesNotExist:
                return Response({'detail': 'not found'}, status=status.HTTP_404_NOT_FOUND)
        
        elif 'owner' in request.GET:
            owner = request.GET['owner']
            if owner is not None:
                ## queryset = queryset.filter(user__id = owner)
                queryset = Order.objects.all()

                serializer = OrderGetSerializer(queryset.filter(owner=owner), many=True)
                return Response({'current_page': current_page, 'per_page': per_page,'data': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'not found'}, status=status.HTTP_404_NOT_FOUND)


        '''elif 'price_min' or 'price_max' pr 'roommates_min' or 'roommates_max' in request.GET:
            queryset=Order.objects.all()
            queryset=queryset.filter(order_type=order_type)
            queryset=queryset[(current_page*per_page):((current_page+1)*per_page)]
            serializer = OrderGetSerializer(queryset, many=True)'''
                
        queryset = Order.objects.all()
        serializer = OrderGetSerializer(queryset[(current_page*per_page):((current_page+1)*per_page)], many=True)
        return Response({'current_page': current_page, 'per_page': per_page,'data': serializer.data}, status=status.HTTP_200_OK)
    
    def post(self, request, format=None):
        serializer = OrderPostSerializer(data=request.data.copy(), partial=True)
        ## Проверяем совпадает ли токен передаваемый пользователем с тем что в настройках, авторизован он или нет.
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        if serializer.is_valid():
            serializer.save()
            return Response({'data': serializer.data}, status=status.HTTP_201_CREATED)
        else:
            return Response({'data': serializer.errors}, status=status.HTTP_206_PARTIAL_CONTENT)


class OrderDetail(APIView):
    def get(self, request, pk, format=None):
        current_page = 0
        if 'current_page' in request.GET:
            current_page = int(request.GET['current_page'])

        per_page = 99999
        if 'per_page' in request.GET:
            per_page = int(request.GET['per_page'])
        
        serializer = OrderGetSerializer(Order.objects.get(id=pk))
        return Response({'data': serializer.data}, status=status.HTTP_200_OK)
    

    def put(self, request, pk, format=None):
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

        try:
            order = Order.objects.get(id=pk)
            serializer = OrderPutSerializer(order, data=request.data.copy(), partial=True)
            if serializer.is_valid():
                serializer.save()
                serializer = OrderGetSerializer(order)
                return Response({'data': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'data': serializer.errors}, status=status.HTTP_206_PARTIAL_CONTENT)
        except Order.DoesNotExist:
            return Response({'detail': 'not found'}, status=status.HTTP_404_NOT_FOUND)
'''
    def post(self, request, format=None):
        serializer = OrderPostSerializer(data=request.data.copy())
        if not settings.DEBUG:
            token = request.META.get('HTTP_TOKEN', '')
            if token != settings.MAINMODULE_TOKEN:
                return Response({'detail': 'unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)

            if serializer.is_valid() in request.data:
                serializer.save()
                return Response({'data': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'invalid request'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'data': serializer.errors}, status=status.HTTP_206_PARTIAL_CONTENT)
class OrderFilter(filters.FilterSet):
    price = django_filters.NumberFilter()
    price__gt = django_filters.NumberFilter(field_name='price', lookup_expr='gte')
    price__lt = django_filters.NumberFilter(field_name='price', lookup_expr='lte')

    roommates = django_filters.NumberFilter(field_name='roommates_needed')
    roommates__gt = django_filters.NumberFilter(field_name='roommates_needed', lookup_expr='gte')
    roommates__lt = django_filters.NumberFilter(field_name='roommates_needed', lookup_expr='lte')

    class Meta:
        model = Order
        fields = ['price', 'roommates_needed']
    

class OrderList(ListAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderGetSerializer
    filterset_fields = ('prices', 'roommates_needed')
    filterset_class = OrderFilter
    
'''

class PushNotification(APIView):
    def get(self, request, format=None):
        device = FCMDevice.objects.all().first()
        title = 'К вам постучались"'
        message = 'Азамат Санатов постучал в вашу дверь'
        device.send_message(title, message)
        return Response({'detail': message}, status=status.HTTP_200_OK)
