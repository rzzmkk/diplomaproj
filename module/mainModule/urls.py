# -*- coding: utf-8 -*-

# future
from __future__ import unicode_literals

# standard library

# third-party
from rest_framework.urlpatterns import format_suffix_patterns

# Django
from django.urls import path, re_path

# local Django
from mainModule.api import *




urlpatterns = [
    re_path(r'^authentication/(?P<pk>[^/]+)/', AuthenticationDetail.as_view()),
    path('authentication/', AuthenticationList.as_view()),

    re_path(r'^user/(?P<pk>[^/]+)/', UserDetail.as_view()),
    path('user/', UserList.as_view()),

    re_path(r'^order/(?P<pk>[^/]+)/', OrderDetail.as_view()),
    path('order/', OrderList.as_view()),
    ## path('order/<pk>', OrderDetail.as_view()),

    re_path(r'^push/(?P<pk>[^/]+)/', PushNotification.as_view()),
    path('push/', PushNotification.as_view()), 
]

urlpatterns = format_suffix_patterns(urlpatterns)

