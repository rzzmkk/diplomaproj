# -*- coding: utf-8 -*-

# future
from __future__ import unicode_literals

# standard library

# third-party
from rest_framework import serializers

# Django

# local Django
from mainModule.models import *

class AuthenticationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Authentication
        fields = ('phone', 'created_at',)


class AuthenticationGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Authentication
        fields = ('id', 'phone',)


class AuthenticationPostSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = Authentication
        exclude = ('password', 'code', 'created_at', 'updated_at',)


class AuthenticationPutSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = Authentication
        exclude = ('created_at', 'updated_at',)


class UserSerializer(serializers.ModelSerializer):
    authentication = AuthenticationGetSerializer()

    class Meta:
        model = User
        fields = ('authentication', 'created_at',)


class UserGetSerializer(serializers.ModelSerializer):
    authentication = AuthenticationGetSerializer()

    class Meta:
        model = User
        fields = '__all__'


class UserPostSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'authentication',)


class UserPutSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = User
        exclude = ('created_at', 'updated_at',)

class OrderGetSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = Order
        fields = '__all__'

class OrderPutSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = Order
        fields = '__all__'

class OrderPostSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = Order
        ## fields = '__all__'
        fields = '__all__'

