from mainModule.models import Order 
import django_filters
## from django_filters import RangeFilter

class OrderFilter(django_filters.FilterSet):
    price = django_filters.NumberFilter()
    price__gt = django_filters.NumberFilter(field_name='price', lookup_expr='gt')
    price__lt = django_filters.NumberFilter(field_name='price', lookup_expr='lt')

    roommates = django_filters.NumberFilter(field_name='roommates_needed')
    roommates__gt = django_filters.NumberFilter(field_name='roommates_needed', lookup_expr='gt')
    roommates__lt = django_filters.NumberFilter(field_name='roommates_needed', lookup_expr='lt')

    class Meta:
        model = Order
        fields = ['price', 'roommates_needed']
