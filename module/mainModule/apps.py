# -*- coding: utf-8 -*-

# future
from __future__ import unicode_literals

# standard library

# third-party

# Django
from django.apps import AppConfig

# local Django

# constant


class MainmoduleConfig(AppConfig):
    name = 'mainModule'
    verbose_name = 'Главная'
