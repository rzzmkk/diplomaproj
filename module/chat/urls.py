# -*- coding: utf-8 -*-

# future
from __future__ import unicode_literals

# standard library

# third-party
from rest_framework.urlpatterns import format_suffix_patterns

# Django
from django.urls import path, re_path

# local Django
from chat import views

urlpatterns = [
    re_path(r'^messages/(?P<pk>[^/]+)//(?P<fk>[^/]+)/', views.message_list),
    path('messages/', views.message_list),
]

urlpatterns = format_suffix_patterns(urlpatterns)
