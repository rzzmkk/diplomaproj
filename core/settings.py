# -*- coding: utf-8 -*-

# future
from __future__ import unicode_literals

# standard library
import os

# third-party
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

# Django

# local Django
from core import config

# constant

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/
sentry_sdk.init(
    dsn=config.DSN,
    integrations=[DjangoIntegration()]
)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config.SECRET_KEY


# SECURITY WARNING: don't run with debug turned on in production!
ALLOWED_HOSTS = config.ALLOWED_HOSTS
DEBUG = config.DEBUG

FILE_UPLOAD_MAX_MEMORY_SIZE = 2621440
DATA_UPLOAD_MAX_MEMORY_SIZE = 2621440
DATA_UPLOAD_MAX_NUMBER_FIELDS = 10240

CACHE_MIDDLEWARE_SECONDS = 0
SESSION_COOKIE_AGE = 21600
LIST_PER_PAGE_SET = 15

USE_DJANGO_JQUERY = True
JQUERY_URL = False


# Application definition
INSTALLED_APPS = [
    'admin_interface',
    'colorfield',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'fcm_django',

    'widget_tweaks',

    'chat',

    'mainModule',
    'django_cron',
    'import_export',
    'rangefilter',
    'rest_framework',
]


FCM_DJANGO_SETTINGS = {
        "FCM_SERVER_KEY": "AAAAXxsbDkc:APA91bGZlUCRXTV8Kfu0nKL6NB3faQJW5zqNHDN-4zIwCb9ZGQxPrXSontlCkkA-K4YGZV4991TcvEqAV-_c2-nDu85W8T_Ctr8FUxibZB44Xm6VoKE2PglpJatqlTTBuEX47KGge48o",
}

FCM_API_KEY = "AAAAAXxsbDkc:APA91bGZlUCRXTV8Kfu0nKL6NB3faQJW5zqNHDN-4zIwCb9ZGQxPrXSontlCkkA-K4YGZV4991TcvEqAV-_c2-nDu85W8T_Ctr8FUxibZB44Xm6VoKE2PglpJatqlTTBuEX47KGge48o"


# Application definition
MAINMODULE_TOKEN = config.MAINMODULE_TOKEN


MIDDLEWARE = [
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
]

REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.MultiPartParser',
    )
}

ROOT_URLCONF = 'core.urls'

TEMPLATES = [
    {
        'BACKEND':
        'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates/'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'core.wsgi.application'


# Database
DATABASES = config.DATABASES


# Cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/
LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Asia/Almaty'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')